//
//  StoreCollectionViewSmallLayout.m
//  MiKiosko
//
//  Created by Euclides Flores on 9/28/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import "StoreCollectionViewSmallLayout.h"
#import "ShelfView.h"

NSString * const StoreCollectionLayoutFooterKind = @"IssueDate";

@interface StoreCollectionViewSmallLayout()
@property (nonatomic, strong) NSDictionary *shelfRects;
@end

@implementation StoreCollectionViewSmallLayout

-(id)init
{
    if (!(self = [super init])) return nil;
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.itemSize = CGSizeMake(200, 260);
    self.sectionInset = UIEdgeInsetsMake(45, 46, 160, 50);
    self.minimumInteritemSpacing = 10.0f;
    self.minimumLineSpacing = 140.0f;
    [self registerClass:[ShelfView class] forDecorationViewOfKind:[ShelfView kind]];
    return self;
}

- (void)prepareLayout
{
    [super prepareLayout];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *footer = [NSMutableDictionary dictionary];
    if (self.scrollDirection == UICollectionViewScrollDirectionVertical) {
        int sectionCount = [self.collectionView numberOfSections];
        CGFloat y = 0;
        CGFloat availableWidth = self.collectionViewContentSize.width - (self.sectionInset.left + self.sectionInset.right);

        int itemsAcross = floorf((availableWidth + self.minimumInteritemSpacing) / (self.itemSize.width + self.minimumInteritemSpacing));
        for (int section = 0; section < sectionCount; section++)
        {
            y += self.headerReferenceSize.height;
            y += self.sectionInset.top;
            
            int itemCount = [self.collectionView numberOfItemsInSection:section];
            int rows = ceilf(itemCount/(float)itemsAcross);
            for (int row = 0; row < rows; row++)
            {
                y += self.itemSize.height;
                dictionary[[NSIndexPath indexPathForItem:row inSection:section]] = [NSValue valueWithCGRect:CGRectMake(0, y - 32, self.collectionViewContentSize.width, 37)];
                
                if (row < rows - 1)
                    y += self.minimumLineSpacing;
            }
            
            y += self.sectionInset.bottom;
            y += self.footerReferenceSize.height;
        }
    } else {
        
        CGFloat y = self.sectionInset.top;
        CGFloat availableHeight = self.collectionViewContentSize.height - (self.sectionInset.top + self.sectionInset.bottom);
        int itemsAcross = floorf((availableHeight + self.minimumInteritemSpacing) / (self.itemSize.height + self.minimumInteritemSpacing));
        CGFloat interval = ((availableHeight - self.itemSize.height) / (itemsAcross <= 1? 1 : itemsAcross - 1)) - self.itemSize.height;
        for (int row = 0; row < itemsAcross; row++) {
            y += self.itemSize.height;
            dictionary[[NSIndexPath indexPathForItem:row inSection:0]] = [NSValue valueWithCGRect:CGRectMake(0, roundf(y - 32), self.collectionViewContentSize.width, 37)];
            y += interval;
        }
    }
    self.shelfRects = [NSDictionary dictionaryWithDictionary:dictionary];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    for (UICollectionViewLayoutAttributes *attributes in array)
    {
        attributes.zIndex = 1;
        if (self.scrollDirection == UICollectionViewScrollDirectionHorizontal && attributes.representedElementCategory == UICollectionElementCategorySupplementaryView)
        {
            // make label vertical if scrolling is horizontal
            attributes.transform3D = CATransform3DMakeRotation(-90 * M_PI / 180, 0, 0, 1);
            attributes.size = CGSizeMake(attributes.size.height, attributes.size.width);    
        }
    }

    NSMutableArray *newArray = [array mutableCopy];
    [self.shelfRects enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (CGRectIntersectsRect([obj CGRectValue], rect))
        {
            UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[ShelfView kind] withIndexPath:key];
            attributes.frame = [obj CGRectValue];

            attributes.zIndex = 0;
            [newArray addObject:attributes];
        }
    }];
    array = [NSArray arrayWithArray:newArray];
    return array;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{

    id shelfRect = self.shelfRects[indexPath];
    if (!shelfRect)
        return nil; // no shelf at this index (this is probably an error)
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[ShelfView kind] withIndexPath:indexPath];
    attributes.frame = [shelfRect CGRectValue];
    attributes.zIndex = 0; // shelves go behind other views
    
    return attributes;
}


@end

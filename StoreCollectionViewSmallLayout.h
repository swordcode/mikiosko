//
//  StoreCollectionViewSmallLayout.h
//  MiKiosko
//
//  Created by Euclides Flores on 9/28/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString * const StoreCollectionLayoutFooterKind;

@interface StoreCollectionViewSmallLayout : UICollectionViewFlowLayout


@end

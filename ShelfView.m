//
//  ShelfView.m
//  MiKiosko
//
//  Created by Euclides Flores on 10/9/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import "ShelfView.h"
#import <QuartzCore/QuartzCore.h>

const NSString *kShelfViewKind = @"ShelfView";

@implementation ShelfView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *imgPath = @"Bookcase_Universal_TopShelf.png";
        iv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgPath]];
        iv.frame = CGRectMake(10, 8, self.frame.size.width - 18, 60);
        iv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        iv.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:iv];
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowOffset = CGSizeMake(0,5);
    }
    return self;
}

- (void)layoutSubviews
{
    CGRect shadowBounds = CGRectMake(iv.frame.origin.x + 4, 52, iv.bounds.size.width - 4, 12);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowBounds].CGPath;
}

+ (NSString *)kind
{
    return (NSString *)kShelfViewKind;
}

@end

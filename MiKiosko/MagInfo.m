//
//  MagInfo.m
//  MiKiosko
//
//  Created by Euclides Flores on 3/4/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import "MagInfo.h"

@implementation MagInfo

@synthesize issueID = _issueID;
@synthesize description = _description;
@synthesize issnID = _issnID;
@synthesize issueDate = _issueDate;
@synthesize timestamp = _timestamp;
@synthesize url= _url;

- (void)dealloc {
    [_issueID release];
    [_description release];
    [_issnID release];
    [_issueDate release];
    [_timestamp release];
    [_url release];
    [super dealloc];
}

@end

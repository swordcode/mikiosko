//
//  CellFooterReusableView.m
//  MiKiosko
//
//  Created by Euclides Flores on 10/14/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import "CellFooterReusableView.h"

@interface CellFooterReusableView()
@property (nonatomic, strong, readwrite) UILabel *issueDate;
@end
@implementation CellFooterReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.issueDate =  [[UILabel alloc] initWithFrame:self.bounds];
        self.issueDate.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.issueDate.backgroundColor = [UIColor clearColor];
        self.issueDate.textAlignment = NSTextAlignmentCenter;
        self.issueDate.font = [UIFont boldSystemFontOfSize:13.0f];
        self.issueDate.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.3f];
        self.issueDate.shadowOffset = CGSizeMake(0.0f, 1.0f);
        [self addSubview:self.issueDate];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.issueDate.text = nil;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

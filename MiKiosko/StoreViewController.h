//
//  FirstViewController.h
//  MiKiosko
//
//  Created by Euclides Flores on 2/27/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreCollectionViewSmallLayout.h"

//#define WEB_SERVICE @"http://localhost:8080/rfMagazineService"
//#define FILE_SERVICE @"http://localhost"
//#define FILE_SERVICE @"http://www.swordcode.com/pauta"

#define WEB_SERVICE @"http://restmagazineapp-swordcode.rhcloud.com/restmagazineapp"
#define FILE_SERVICE @"http://localhost"

@interface StoreViewController : UIViewController <UIAlertViewDelegate, NSURLConnectionDownloadDelegate, UICollectionViewDataSource>
{
    NSArray *storeObjects;
    UIPopoverController *popOverController;
}

@property (weak, nonatomic) UIPopoverController *popOverController;
@property (retain, nonatomic) IBOutlet UIToolbar *editBtn;
@property (retain, nonatomic) IBOutlet UIToolbar *toolbar;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *settingBtn;
@property (retain, nonatomic) IBOutlet UICollectionView *storeCollectionView;


@end

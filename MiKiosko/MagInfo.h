//
//  MagInfo.h
//  MiKiosko
//
//  Created by Euclides Flores on 3/4/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MagInfo : NSObject {
    NSNumber *_issueID;
    NSString *_description;
    NSString *_issnID;
    NSDate *_issueDate;
    NSDate *_timestamp;
    NSString *_url;
}

@property (nonatomic, retain) NSNumber *issueID;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *issnID;
@property (nonatomic, retain) NSDate *issueDate;
@property (nonatomic, retain) NSDate *timestamp;
@property (nonatomic, retain) NSString *url;
@end

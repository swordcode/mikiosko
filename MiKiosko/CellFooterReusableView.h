//
//  CellFooterReusableView.h
//  MiKiosko
//
//  Created by Euclides Flores on 10/14/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellFooterReusableView : UICollectionReusableView
@property (nonatomic, strong, readonly) UILabel *issueDate;
@end

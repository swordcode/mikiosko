//
//  SettingViewController.h
//  MiKiosko
//
//  Created by Euclides Flores on 4/18/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UITableViewController

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@end

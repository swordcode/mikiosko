//
//  main.m
//  MiKiosko
//
//  Created by Euclides Flores on 2/27/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

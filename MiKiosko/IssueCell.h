//
//  IssueCell.h
//  MiKiosko
//
//  Created by Euclides Flores on 9/24/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@class IssueCell;

@interface IssueCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) UIActivityIndicatorView *ai;
- (void)setCoverImage:(NSURL *)remoteImage;
@end

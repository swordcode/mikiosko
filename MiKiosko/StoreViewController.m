//
//  FirstViewController.m
//  MiKiosko
//
//  Created by Euclides Flores on 2/27/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import "StoreViewController.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "MagInfo.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import <QuartzCore/QuartzCore.h>
#import "SettingViewController.h"
#import <NewsstandKit/NewsstandKit.h>
#import "PDColoredProgressView.h"
#import "RKXMLReaderSerialization.h"
#import "IssueCell.h"
#import "CellFooterReusableView.h"

@interface StoreViewController ()
@property (nonatomic, strong) StoreCollectionViewSmallLayout *smallLayout;
@end

@implementation StoreViewController
@synthesize popOverController = popOverController;

//track the cell that is being used
int cellid = 0;
NSMutableArray *sectionsInGrid;
static NSString * const AlbumTitleIdentifier = @"IssueDate";

- (void)loadView
{
    sectionsInGrid = [[NSMutableArray alloc] init];
    self.smallLayout = [[StoreCollectionViewSmallLayout alloc] init];
    [super loadView];
    self.storeCollectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Bookcase_Universal_TOP"]];
    [self.storeCollectionView setCollectionViewLayout:self.smallLayout animated:true];
}

- (void)loadStoreInfo
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Cargando Tienda";
    NSURL *url = [NSURL URLWithString:WEB_SERVICE];
    RKLogConfigureByName("RestKit", RKLogLevelError);
    RKLogConfigureByName("RestKit/Network", RKLogLevelError);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelError);
    [RKMIMETypeSerialization registerClass:[RKXMLReaderSerialization class] forMIMEType:@"application/xml"];
    RKObjectMapping *magInfoMapping = [RKObjectMapping mappingForClass:[MagInfo class]];
    [magInfoMapping addAttributeMappingsFromDictionary:@{
        @"issueId": @"issueID",
        @"description": @"description",
        @"issnId": @"issnID",
        @"issueDate": @"issueDate"
    }];

    RKResponseDescriptor *rd = [RKResponseDescriptor responseDescriptorWithMapping:magInfoMapping pathPattern:nil keyPath:nil statusCodes:[NSIndexSet indexSetWithIndex:200]];
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:url];
    [objectManager addResponseDescriptor:rd];
    [objectManager getObjectsAtPath:@"webresources/com.swordcode.maginfo" parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                storeObjects = [[mappingResult array] retain];
                                [self.storeCollectionView reloadData];
                                [sectionsInGrid removeAllObjects];
                                cellid = 0;
                                [self hideHud];
                                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                [self hideHud];
                                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void) printArray {
    for (id obj in storeObjects) {
        if ([obj class] == [MagInfo class]) {
            MagInfo* magInfo = (MagInfo *)obj;
            NSLog(@"%@", magInfo.issnID);
        }
    }
}

- (void) hideHud {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)viewDidLoad
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Bookcase_Universal_TOP.png"]];
    [self loadStoreInfo];
    [super viewDidLoad];
    UINavigationController *Controller = [[UINavigationController alloc] init];
    SettingViewController *content = [[SettingViewController alloc] init];
    content =[[UIStoryboard storyboardWithName:@"MainStoryboard"
                                                bundle:nil] instantiateViewControllerWithIdentifier:@"rpSetting"];
    Controller.viewControllers=[NSArray arrayWithObject:content];
    content.contentSizeForViewInPopover = CGSizeMake(320, 346);
    self.popOverController = [[UIPopoverController alloc] initWithContentViewController:Controller];
    Controller.navigationItem.title = @"Preferencias";
    Controller.title = @"Preferencias";
    [_settingBtn setTarget:self];
    [_settingBtn setAction:@selector(showPopOver:)];
    [self.storeCollectionView registerClass:[CellFooterReusableView class]
            forSupplementaryViewOfKind:StoreCollectionLayoutFooterKind
                   withReuseIdentifier:AlbumTitleIdentifier];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [storeObjects release];
    [_editBtn release];
    [_toolbar release];
    [sectionsInGrid release];
    [popOverController release];
    [_settingBtn release];
    [_storeCollectionView release];
    [_smallLayout release];
    [super dealloc];
}


- (IBAction)showPopOver:(id)sender {
    [self.popOverController presentPopoverFromBarButtonItem:self.settingBtn
                                   permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark Download button
- (IBAction)downloadButtonPressed:(id)sender {
//    UICollectionViewCell *clickedCell = (UICollectionViewCell *)[[sender superview] superview];
    id cell = [[sender superview] superview];
    NSString *className = NSStringFromClass([cell class]);
    NSLog(@"what class is this ??? %@", className);
    UIButton *btn = (UIButton *)sender;
    MagInfo *magInfo = [storeObjects objectAtIndex:[btn tag]];
    if (magInfo != nil) {
        NKLibrary *library = [NKLibrary sharedLibrary];
        if (![library issueWithName:magInfo.issnID]) {
            NKIssue *issue = [library addIssueWithName:magInfo.issnID date:magInfo.issueDate];
            NSString *url = [NSString stringWithFormat:@"%@/%@.zip", FILE_SERVICE, magInfo.issnID];
            NKAssetDownload *asset = [issue addAssetWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
            [asset setUserInfo:[NSDictionary dictionaryWithObject:magInfo.issueID forKey:@"issnID"]];
            [asset downloadWithDelegate:self];
        }
    }
}

#pragma mark NSURLConnectionDownload delegates

- (void)connection:(NSURLConnection *)connection didWriteData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long) expectedTotalBytes {
}

- (void)connectionDidResumeDownloading:(NSURLConnection *)connection totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long) expectedTotalBytes {
}


- (void)connectionDidFinishDownloading:(NSURLConnection *)connection destinationURL:(NSURL *) destinationURL {
	
}


#pragma mark UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [storeObjects count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IssueCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell viewWithTag:100];
    activityIndicator.alpha = 1.0f;
    [activityIndicator setHidden:false];
    [activityIndicator startAnimating];
    MagInfo *magInfo = (MagInfo *)[storeObjects objectAtIndex:indexPath.row];
    NSURL *remoteImage = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@.jpg", FILE_SERVICE, magInfo.issnID]];
    SDImageCache *imageCache = [SDImageCache.alloc initWithNamespace:@"kiosko"];
    UIImageView *cellImage =  (UIImageView *)[cell viewWithTag:101];
    [imageCache queryDiskCacheForKey:[remoteImage absoluteString] done:^(UIImage *image, SDImageCacheType cacheType) {
        if (image != nil) {
            //Fix-Me: Implement me
        } else {
            [cellImage setImageWithURL:remoteImage placeholderImage:nil options:SDWebImageDownloaderProgressiveDownload progress:^(NSUInteger receivedSize, long long expectedSize) {
            } completed:^(UIImage *newImage, NSError *error, SDImageCacheType cacheType) {
                if (error) {
                    NSLog(@"Error downloading magazine cover image: %@", error);
                } else {
                    [UIView animateWithDuration:0.5 animations:^{
                        activityIndicator.alpha = 0.0;
                    } completion:^(BOOL finished){
                        [activityIndicator stopAnimating];
                        [[SDImageCache sharedImageCache] storeImage:newImage forKey:[remoteImage absoluteString]];
                    }];
                }
            }];
        }
    }];
    return cell;
}

- (UITextView *) createIssueDate:(float)x:(NSString *)text:(int)tag
{
    UITextView *view = [[UITextView alloc] init];
    view.tag = tag;
    view.frame = CGRectMake(x, 26.0, 90.0, 22.0);
    view.backgroundColor = [UIColor clearColor];
    view.textAlignment = NSTextAlignmentCenter;
    view.editable = NO;
    view.userInteractionEnabled = NO;
    [view setFont:[UIFont fontWithName:@"Arial" size:13.0]];
    view.text = text;
    return view;
}

- (PDColoredProgressView *) addProgressView:(float)x:(int)tag
{
    PDColoredProgressView *progressView = [[PDColoredProgressView alloc] initWithProgressViewStyle: UIProgressViewStyleDefault];
    [progressView setTintColor: [UIColor greenColor]];
    [progressView setHidden:true];
    [progressView setTag:tag];
    progressView.frame = CGRectMake(x, 56.0, 140.0, 7.0);
    return progressView;
}

- (UIButton *) createButton:(float)x:(int)tag
{
    UIImage *btnImage = [[UIImage imageNamed:@"greenButton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 28, 18, 18)];
    UIImage *btnImageHl = [[UIImage imageNamed:@"greenButtonHighlight.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 28, 18, 18)];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTag:tag];
    [btn setTitle:@"Descargar" forState:UIControlStateNormal];
    [btn setBackgroundImage:btnImage forState:UIControlStateNormal];
    [btn setBackgroundImage:btnImageHl forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Arial" size:13.0]];
    btn.contentMode = UIViewContentModeScaleToFill;
    btn.frame = CGRectMake(x, 28.0, 90.0, 22.0);
    btn.layer.cornerRadius = 1.0f;
    btn.layer.masksToBounds = NO;
    btn.layer.borderWidth = 0.0f;
    btn.layer.shadowColor = [UIColor grayColor].CGColor;
    btn.layer.shadowOpacity = 0.9;
    btn.layer.shadowRadius = 1;
    btn.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    return btn;
}

#pragma marc Misc methods
- (NSString *)getMonthInSpanish:(int)monthCode {
    switch (monthCode) {
        case 1:
            return @"Enero";
            break;
        case 2:
            return @"Febrero";
            break;
        case 3:
            return @"Marzo";
            break;
        case 4:
            return @"Abril";
            break;
        case 5:
            return @"Mayo";
            break;
        case 6:
            return @"Junio";
            break;
        case 7:
            return @"Julio";
            break;
        case 8:
            return @"Agosto";
            break;
        case 9:
            return @"Septiembre";
            break;
        case 10:
            return @"Octubre";
            break;
        case 11:
            return @"Noviembre";
            break;
        default:
            return @"Diciembre";
    }
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Isabella Mi Niña");
    CellFooterReusableView *view = [self.storeCollectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:AlbumTitleIdentifier forIndexPath:indexPath];
    view.issueDate.text = @"Lolita 9";
    return view;
}

@end

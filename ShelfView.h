//
//  ShelfView.h
//  MiKiosko
//
//  Created by Euclides Flores on 10/9/13.
//  Copyright (c) 2013 Swordcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShelfView : UICollectionReusableView {
    UIImageView *iv;
}

+ (NSString *)kind;
@end
